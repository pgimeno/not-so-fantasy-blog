# Development Blog for GBA retro-dev system

This repository is actually a development blog for a system similar to fantasy retro-console apps (Liko-12, Pico-8...) that will generate code for Nintendo's not-so-fantasy console Game Boy Advance.

### Blog

The blog posts are issues posted to the issue tracker. You can read old entries by looking at the closed issues. Issue #1 contains the current project status and possibly other relevant information, and will always be open. Other than that, the last blog entry will be the only other open issue; as a new one is created, the previous one will be closed.

[Blog](/pgimeno/not-so-fantasy-blog/issues)<br>
[Old entries](/pgimeno/not-so-fantasy-blog/issues?state=closed)

### Project Description

The RetroAdvance project implements a retro-console in the style of the LIKO-12 and PICO-8 fantasy consoles; but instead of being designed so that everything runs on the same system with the same app, or in a web page in the case of PICO-8, it can be developed on any modern computer, and generates a ROM that will run on the Game Boy Advance (or an emulator, of course).

The programming language for game creators is [Nelua](https://nelua.io/).

The project repository is here:

[Main Project](/pgimeno/RetroAdvance)

### License

The license is ISC.

### The ISC license

(C) Copyright 2021 Pedro Gimeno Fortea

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
