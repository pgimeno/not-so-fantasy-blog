import sys
import PIL.Image

def rgb8to5(b, g, r):
  #s = int(s, 16)
  #r, g, b = s >> 16 & 0xFF, s >> 8 & 0xFF, s & 0xFF

  # scale to max range supported
  s = float(0xFB)/255.
  r, g, b = r*s, g*s, b*s

  # apply rounding to nearest or even
  s = float(1<<55)
  r = int(r + s - s)
  g = int(g + s - s)
  b = int(b + s - s)

  #return ('00000'+hex((r<<16)+(g<<8)+b)[2:])[-6:]
  return (b << 7) + (g << 2) + (r >> 3)


def main():
  img = PIL.Image.open(sys.argv[1])

  if img.palette is None:
    raise Exception("Not a palette image")

  if img.width != 256:
    raise Exception("Width should be 256 px")

  if img.height > 128:
    raise Exception("Image too high, maximum 128 vertical px")

  if (img.height & 7) != 0:
    raise Exception("Vertical height not a multiple of 8")

  palette = bytearray(img.palette.palette)
  bitmap = bytearray(img.getdata())

  # Reroganize palette
  outpal = bytearray()
  for i in range(0, len(palette), 3):
    entry = rgb8to5(palette[i+2], palette[i+1], palette[i])
    outpal.append(entry & 0xFF)
    outpal.append(entry >> 8)

  f = open('palette.bin', 'wb')
  f.write(outpal)
  f.close()

  outdata = bytearray()
  for y in range(img.height>>3):  # As many tiles vertically as the image has
    for x in range(32):  # 32 tiles horizontally
      for ty in range(8):  # 8 rows per tile
        for tx in range(0, 8, 2):  # 8 pixels per tile, 2 pixels per byte
          ofs = y*256*8 + x*8 + ty*256 + tx
          outdata.append(bitmap[ofs] + bitmap[ofs+1]*16)

  f = open('tileimage.bin', 'wb')
  f.write(outdata)
  f.close()

  sys.stderr.write("Successfully generated palette.bin and tileimage.bin\n")

if __name__ == '__main__':
  ret = main()
  if ret:
    sys.exit(ret)
