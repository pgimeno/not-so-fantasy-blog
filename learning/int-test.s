		.cpu arm7tdmi
		.text
		.global	_start

		.arm
_start:
		b realstart

		.include "header.inc"

joybus:		.arm
		swi	3		@ Unhandled here - stop
		b	joybus

		.include "gba_constants.inc"
		.include "gba_moreconstants.inc"

		.balign 4
ISR:		.arm
		mov	r1,#INTR_FLAG_VBLANK	@ VBLANK bitmask
		ldr	r0,=#BIOS_INT_ACK	@ BIOS int acknowledge address
		ldrh	r2,[r0]
		orr	r2,r1		@ Acknowledge VBLANK only
		strh	r2,[r0]
		ldr	r0,=#REG_IF
		strh	r1,[r0]		@ Acknowledge it to the hardware
		bx	lr
		.ltorg

realstart:	.arm
		add	r0,pc,#1
		bx	r0		@ Switch to Thumb mode
		.thumb

		@ Set mode 3 and enable BG 2
		ldr	r1,=#REG_DISPCNT
		ldr	r0,=#DC_BG2EN|DC_FRAME0|DC_MODE_3
		str	r0,[r1]

		@ Disable master interrupt
		ldr	r1,=#REG_IME		@ master enable
		mov	r0,#0
		strh	r0,[r1]			@ Master Interrupt disable

		@ Set up interrupt vector
		ldr	r0,=#ISR		@ Our interrupt handler
		ldr	r1,=#BIOS_INT_VEC		@ BIOS interrupt vector pointer
		str	r0,[r1]			@ Set interrupt vector

		@ Enable VBlank IRQ in graphics processor
		ldr	r1,=#REG_DISPSTAT
		ldr	r0,[r1]
		mov	r2,#DS_EN_MASK		@ clear bits 3, 4, 5
		bic	r0,r2
		mov	r2,#DS_VBLANK_EN	@ set bit 3 (VBlank)
		orr	r0,r2
		str	r0,[r1]			@ DISPSTAT = 8

		@ Enable VBlank IRQ in interrupt processor
		ldr	r1,=#REG_IE
		ldrh	r0,[r1]
		mov	r2,#INTR_FLAG_VBLANK	@ Set bit 0 (VBlank interrupt)
		orr	r0,r2
		strh	r0,[r1]

		@ Enable interrupts in master interrupt enable register
		ldr	r1,=#REG_IME		@ 04000208 = master enable
		mov	r0,#1
		strh	r0,[r1]			@ Master Interrupt enable

		@ Now perform a simple RAM fill loop

		ldr	r6,=#VRAM+2*240*140-2	@ limit: line 140 out of 160

		ldr	r0,=#31		@ R=31, G=0, B=0
		strh	r0,[r7]		@ store snitch pixel
		@ this pixel will tell us if swi hangs
		swi	5		@ check if interrupts are working

2:
		swi	5

		@ Fill VRAM with a single colour
		ldr	r0,=#0b0000001111100000	@ RGB: R=0, G=31, B=0
		ldr	r1,=#VRAM

		.balign 4
1:		strh	r0,[r1]		@ store pixel
		add	r1,#2		@ next address
		cmp	r1,r6		@ are we done yet?
		bls	1b		@ nope? jump, yes? fall through

		swi	5

		@ Do it again with a different colour
		ldr	r0,=#0b0000000000011111	@ RGB: R=31, G=0, B=0
		ldr	r1,=#VRAM

		.balign 4
1:		strh	r0,[r1]
		add	r1,#2
		cmp	r1,r6
		bls	1b

		b	2b

		.ltorg
