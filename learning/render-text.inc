		.text
		.balign	2
		.thumb

# screen/buffer start address in r6
# text pointer in r7
# colour in r5
# does not wrap around properly
# does not check if out of limits
render_text:
		push	{r0-r4,r7}

		ldr	r0,=#REG_DISPCNT
		ldr	r0,[r0]
		mov	r1,#7
		and	r0,r1
		cmp	r0,#3
		beq	3f
		cmp	r0,#5
		beq	5f

9:		@ finish and return
		pop	{r0-r4,r7}
		bx	lr

		.ltorg

render_text_mode_5:
		push	{r0-r4,r7}

5:
		ldr	r3,=#(1-160*7)*2
		ldr	r2,=#(160-7)*2
		b	8f

render_text_mode_3:
		push	{r0-r4,r7}

3:
		ldr	r3,=#(1-240*7)*2
		ldr	r2,=#(240-7)*2

8:		@ 3+5

# registers:
# r0: character bitmap address
# r1: bits from bitmap
# r2 = screen address increment to go to next scanline from right edge
# r3 = screen address increment to go to next character from bottom right
# r4 = font base address
# r5 = input colour
# r6 = moving pixel address
# r7 = moving text pointer

		ldr	r4,=#Font8x8

0:		@ Next Char
		ldrb	r0,[r7]		@ grab next characters
		lsl	r0,#3		@ 8 bytes per character
		beq	9b		@ null = end of string
		add	r7,#1		@ inc text pointer
		add	r0,r4		@ add font base

		ldr	r1,[r0,#4]	@ top half of char (they are flipped)

		@ All 64 pixels are unrolled
		.rept	4

		.rept	7
		add	r1,r1
		bcc	.+4
		strh	r5,[r6]
		add	r6,#2		@ move one pixel right
		.endr

		add	r1,r1
		bcc	.+4
		strh	r5,[r6]
		add	r6,r2		@ next scanline's screen address

		.endr

		ldr	r1,[r0,#0]	@ bottom half of char

		.rept	3

		.rept	7
		add	r1,r1
		bcc	.+4
		strh	r5,[r6]
		add	r6,#2
		.endr

		add	r1,r1
		bcc	.+4
		strh	r5,[r6]
		add	r6,r2		@ next scanline's screen address

		.endr

		.rept	7
		add	r1,r1
		bcc	.+4
		strh	r5,[r6]
		add	r6,#2
		.endr

		add	r1,r1
		bcc	.+4
		strh	r5,[r6]

		add	r6,r3		@ next character's screen address
		b	0b		@ Next Char

		.ltorg



Font8x8:
		.incbin	"font.bin"

# TODO
coord_to_adr:
		bx	lr

# R7: number
# stores in NumBuf
hex_str:	push	{r6,lr}
		mov	r6,r7
		ldr	r7,=#NumBuf
		bl	hex_str_buf
		mov	r7,r6
		pop	{r6,pc}
# R6: number
# R7: buffer (min 9 bytes). Adds trailing \0.
hex_str_buf:
		push	{r0,r1,r7,lr}
		mov	r0,#0xF
		mov	r1,r6
		lsr	r1,#28
		bl	1f
		mov	r1,r6
		lsr	r1,#24
		bl	1f
		mov	r1,r6
		lsr	r1,#20
		bl	1f
		mov	r1,r6
		lsr	r1,#16
		bl	1f
		mov	r1,r6
		lsr	r1,#12
		bl	1f
		mov	r1,r6
		lsr	r1,#8
		bl	1f
		mov	r1,r6
		lsr	r1,#4
		bl	1f
		mov	r1,r6
		bl	1f
		mov	r1,#0
		str	r1,[r7]
		pop	{r0,r1,r7,pc}

1:		and	r1,r0
		add	r1,#'0'
		cmp	r1,#'9'
		bls	.+4
		add	r1,#'A'-'9'-1
		strb	r1,[r7]
		add	r7,#1
		bx	lr

# TODO
# R6: number
# R7: buffer (min 11 bytes). Adds trailing \0.
dec_str:
		bx	lr

		.section .ewram
NumBuf:		.space	11

