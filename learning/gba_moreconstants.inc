@ Addresses relevant to BIOS
BIOS_INT_ACK	= 0x03FFFFF8
BIOS_INT_VEC	= 0x03FFFFFC

@ Bits of registers

@ DISPCNT bits
DC_BG0EN	= (1<<8)
DC_BG1EN	= (1<<9)
DC_BG2EN	= (1<<10)
DC_BG3EN	= (1<<11)
DC_OBJEN	= (1<<12)
DC_OBJ_2D	= (0<<6)
DC_OBJ_1D	= (1<<6)
DC_FRAME0	= (0<<4)
DC_FRAME1	= (1<<4)
DC_MODE_0	= 0
DC_MODE_1	= 1
DC_MODE_2	= 2
DC_MODE_3	= 3

@ DISPSTAT bits

DS_VBLANK_FLAG	= (1<<0)
DS_HBLANK_FLAG	= (1<<1)
DS_VCOUNT_FLAG	= (1<<2)
DS_VBLANK_EN	= (1<<3)
DS_HBLANK_EN	= (1<<4)
DS_VCOUNT_EN	= (1<<5)
DS_EN_MASK	= (DS_VBLANK_EN|DS_HBLANK_EN|DS_VCOUNT_EN)
DS_VCOUNT	= (1<<8)	@ Multiply by VCount setting

@ BGxCNT bits

@ Regular BG sizes in modes 0-2
RSIZE_256x256	= (0<<14)
RSIZE_512x256	= (1<<14)
RSIZE_256x512	= (2<<14)
RSIZE_512x512	= (3<<14)
@ Affine BG sizes in modes 0-2
ASIZE_128x128	= (0<<14)
ASIZE_256x256	= (1<<14)
ASIZE_512x512	= (2<<14)
ASIZE_1024x1024	= (3<<14)

@ Base address for tile map
MAP_0000	= (0<<8)
MAP_0800	= (1<<8)
MAP_1000	= (2<<8)
MAP_1800	= (3<<8)
MAP_2000	= (4<<8)
MAP_2800	= (5<<8)
MAP_3000	= (6<<8)
MAP_3800	= (7<<8)
MAP_4000	= (8<<8)
MAP_4800	= (9<<8)
MAP_5000	= (10<<8)
MAP_5800	= (11<<8)
MAP_6000	= (12<<8)
MAP_6800	= (13<<8)
MAP_7000	= (14<<8)
MAP_7800	= (15<<8)
MAP_8000	= (16<<8)
MAP_8800	= (17<<8)
MAP_9000	= (18<<8)
MAP_9800	= (19<<8)
MAP_A000	= (20<<8)
MAP_A800	= (21<<8)
MAP_B000	= (22<<8)
MAP_B800	= (23<<8)
MAP_C000	= (24<<8)
MAP_C800	= (25<<8)
MAP_D000	= (26<<8)
MAP_D800	= (27<<8)
MAP_E000	= (28<<8)
MAP_E800	= (29<<8)
MAP_F000	= (30<<8)
MAP_F800	= (31<<8)
@ 16- or 256- colour palette
BG_PAL16	= (0<<7)
BG_PAL256	= (1<<7)
@ Tile Image
TLIMG_0000	= (0<<2)
TLIMG_4000	= (1<<2)
TLIMG_8000	= (2<<2)
TLIMG_C000	= (3<<2)
@ Priority
BG_PRIO_0	= 0
BG_PRIO_1	= 1
BG_PRIO_2	= 2
BG_PRIO_3	= 3


@ OAM
OAM0_AFF	= (1<<8)
OAM0_DISABLE	= (1<<9)
OAM0_NORMAL	= (0<<10)
OAM0_SEMITRANS	= (1<<10)
OAM0_WINDOW	= (2<<10)
OAM0_MOSAIC	= (1<<12)
OAM0_PAL16	= (0<<13)
OAM0_PAL256	= (1<<13)
OAM0_SQUARE	= (0<<14)
OAM0_HORIZ	= (1<<14)
OAM0_VERT	= (2<<14)

OAM1_AFFPARAM	= (1<<9)	@ Multiply by affine transform parameter
OAM1_HFLIP	= (1<<12)
OAM1_VFLIP	= (1<<13)
OAM1_SIZE_0	= (0<<14)
OAM1_SIZE_1	= (1<<14)
OAM1_SIZE_2	= (2<<14)
OAM1_SIZE_3	= (3<<14)

OAM2_PRIO_0	= (0<<10)
OAM2_PRIO_1	= (1<<10)
OAM2_PRIO_2	= (2<<10)
OAM2_PRIO_3	= (3<<10)
OAM2_PALETTE	= (1<<12)	@ Multiply by palette number

