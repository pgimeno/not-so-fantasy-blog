		.cpu arm7tdmi
		.text
		.global	_start

		.arm
_start:
		b realstart

		.include "header.inc"

		.bss
		.balign	4

NWords = 16
HeapEnd = 0x02040000

		@ This is a map of occupied/free words.
		@ A 1 in a halfword indicates the word belongs to allocated
		@ space; a 0 indicates it belongs to free space.
RAMmap:		.space	NWords*4
RAMmapEnd:

RAMmap2:	.space	NWords*4


		@ Occupy all positions in EWRAM except the last NWords*4
		@ NOTE: This is an offset WITHIN the section!
		.org	0x040000-NWords*4

		.text
joybus:		.arm
		swi	3		@ Unhandled here - stop
		b	joybus

		.include "gba_constants.inc"
		.include "gba_moreconstants.inc"

realstart:	.arm
		add	r0,pc,#1
		bx	r0		@ Switch to Thumb mode
		.thumb

		.global	memmgr_GetMem
		.global	memmgr_FreeMem
		.global	malloc
		.global	free

		ldr	r0,=#DC_BG2EN|DC_FRAME0|DC_MODE_3
		ldr	r1,=#REG_DISPCNT
		str	r0,[r1]

		bl	memmgr_Init
		bl	InitMap

		bl	Check

		ldr	r0,=#0x40
		bl	GetMemChk

		mov	r4,r0
		add	r0,#4
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#16
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#8
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#12
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#20
		mov	r1,#44
		bl	FreeMemChk

		mov	r0,r4
		mov	r1,#4
		bl	FreeMemChk

		@ Empty at this point

		ldr	r0,=#0x40
		bl	GetMemChk

		@ Full again

		mov	r0,r4
		add	r0,#0x3C
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,#4
		bl	GetMemChk

		@ Full again

		mov	r0,r4
		add	r0,#0x38
		mov	r1,#8
		bl	FreeMemChk

		mov	r0,#8
		bl	GetMemChk

		@ Full again

		mov	r0,r4
		add	r0,#0x38
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x3C
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,#8
		bl	GetMemChk

		@ Full again

		mov	r0,r4
		add	r0,#0x30
		mov	r1,#8
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x38
		mov	r1,#8
		bl	FreeMemChk

		mov	r0,#0x10
		bl	GetMemChk

		@ Full again

		mov	r0,r4
		add	r0,#0x28
		mov	r1,#8

		bl	FreeMemChk

		mov	r0,r4
		mov	r1,#4
		bl	FreeMemChk
		mov	r0,#4
		bl	GetMemChk

		mov	r0,r4
		mov	r1,#8
		bl	FreeMemChk
		mov	r0,#8
		bl	GetMemChk

		mov	r0,r4
		add	r0,#4
		mov	r1,#8
		bl	FreeMemChk
		mov	r0,#8
		bl	GetMemChk

		mov	r0,r4
		mov	r1,#0x28
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x30
		mov	r1,#0xC
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x3C
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,#0x40
		bl	GetMemChk

		@ All full again

		mov	r0,r4
		add	r0,#0x20
		mov	r1,#8
		bl	FreeMemChk

		mov	r0,r4
		mov	r1,#8
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#8
		mov	r1,#8
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x18
		mov	r1,#8
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x10
		mov	r1,#8
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x28
		mov	r1,#0x40-0x28
		bl	FreeMemChk

		mov	r0,#0x40
		bl	GetMemChk

		@ All full again

		mov	r0,r4
		add	r0,#4
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x3C
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x10
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x20
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x1C
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x14
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		add	r0,#0x18
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,r4
		mov	r1,#4
		bl	FreeMemChk

		mov	r0,#0x24-0x10
		bl	GetMemChk

		mov	r0,#8
		bl	GetMemChk

		mov	r0,#4
		bl	GetMemChk

		mov	r0,r4
		mov	r1,#0x40
		bl	FreeMemChk

		@ All empty

		@ Finish line (Success)
		ldr	r3,=#0b0000001111100000
		b	FillScrAndHalt




		.ltorg

		@ Shortcuts to GetMem and FreeMem in thumb mode
GetMem:		.thumb
		push	{lr}
		mov	r1,#1
		mov	lr,r1
		ldr	r1,=#memmgr_GetMem
		add	lr,pc
		bx	r1
		pop	{r1}
		bx	r1

FreeMem:	.thumb
		push	{lr}
		mov	r2,#1
		mov	lr,r2
		ldr	r2,=#memmgr_FreeMem
		add	lr,pc
		bx	r2
		pop	{r1}
		bx	r1

		.ltorg


@ Memory manager internals
		.global	memmgr_FreeList
		.global	__bss_end__
FreeList	= memmgr_FreeList
HeapOrg		= __bss_end__


InitMap:	ldr	r0,=#0
		ldr	r1,=#RAMmap
		ldr	r2,=#NWords
1:		strh	r0,[r1]
		add	r1,#4
		sub	r2,#1
		bhi	1b
		bx	lr

@ Similar to GetMem but updates our RAMmap instead
MapGetMem:	cmp	r0,#0
		beq	mgmRet
		push	{r4-r5}
		add	r0,#3
		mov	r2,#3
		bic	r0,r2
		ldr	r1,=#RAMmap
		ldr	r2,=#RAMmapEnd
		@ Find free position
mgmFindFree:	ldmia	r1!,{r3}
		cmp	r3,#0
		beq	mgmFoundHole
mgmFindFreeChk:	cmp	r1,r2
		blo	mgmFindFree
mgmFindFreeErr:	sub	r0,r0
		pop	{r4-r5}
mgmRet:		bx	lr

mgmFoundHole:
		sub	r5,r1,#4	@ remember start position of hole
		sub	r4,r0,#4	@ copy length to count holes in r4
					@ (found 1 already)
mgmCheckFits:	beq	mgmFoundRoom	@ if length exhausted, we've found it
		cmp	r1,r2
		bhs	mgmFindFreeErr	@ if past limits, error
		ldmia	r1!,{r3}
		cmp	r3,#0
		bne	mgmFindFreeChk	@ if occupied, keep searching
		sub	r4,#4
		b	mgmCheckFits

mgmFoundRoom:	mov	r2,r0		@ length
		mov	r0,r5		@ return value = remembered position
		mov	r1,#1
mgmSetAlloc:	stmia	r5!,{r1}
		sub	r2,#4
		bne	mgmSetAlloc
		pop	{r4-r5}
		bx	lr

		@ Test if a MapFreeMem of the requested block would be valid
		@ (Q for Query)
MapFreeMemQ:	add	r1,#3
		mov	r2,#3
		bic	r1,r2
		cmp	r1,#0
		beq	mfmqBadFree	@ length 0, bad
		cmp	r0,#0
		beq	mfmqGoodFree	@ null pointer, good
		ldr	r2,=#HeapOrg
		sub	r0,r2
		blo	mfmqBadFree
		ldr	r2,=#RAMmap
		add	r0,r2		@ r0 = corresponding position in map
		ldr	r3,=#RAMmapEnd
1:		cmp	r0,r3
		bhs	mfmqBadFree
		ldmia	r0!,{r2}
		cmp	r2,#0
		beq	mfmqBadFree
		sub	r1,#4
		bhi	1b
mfmqGoodFree:	movs	r0,#1
		add	r0,#0		@ clear carry
		bx	lr

mfmqBadFree:	sub	r0,r0		@ set carry
		bx	lr

@ Like MapFreeMemQ but silent, and updating the map
MapFreeMem:	cmp	r0,#0
		beq	2f
		ldr	r2,=#RAMmapEnd-HeapEnd
		add	r0,r2
		ldr	r2,=#0
1:		stmia	r0!,{r2}
		sub	r1,#4
		bhi	1b
2:		bx	lr

		.ltorg

		@ Test if the memory manager's free list and the miniature
		@ memory map coincide. Tests also the consistency of the
		@ free list.
compareWithMap:	push	{r4-r5}
		@ Pointers to both arrays
		ldr	r4,=#RAMmap
		ldr	r5,=#HeapOrg

		ldr	r1,=#FreeList
		ldr	r1,[r1]		@ grab data at FreeList
		mov	r2,#3
		tst	r1,r2
		bne	cwmBad		@ bits 0-1 should be clear, else error
		ldr	r2,=#HeapEnd
		cmp	r1,r2
		bhi	cwmBad		@ if [FreeList] > HeapEnd, bad
		sub	r1,r5
		blo	cwmBad		@ if [FreeList] < HeapOrg, bad
		beq	cwmNoLeadBlock	@ if [FreeList] = HeapOrg, no l.block
		@ r1 = index of first free
		mov	r0,#0		@ start at index 0
1:		ldrh	r2,[r4,r0]
		add	r0,#4
		cmp	r2,#0		@ check if all of them are allocated
		beq	cwmBad		@ empty cell found means error
		cmp	r0,r1		@ while running index < ind.of empty
		blo	1b
		bne	cwmBad		@ consistency check
cwmNoLeadBlock:
		@ at this point, previous pointer and map index are in sync,
		@ and r1 is the common index and points to a free list entry
		@ or end
		ldr	r2,=#HeapEnd
		add	r0,r5,r1	@ pointer into heap
		cmp	r0,r2
		beq	cwmGood		@ if end, all fine
		bhi	cwmBad		@ but never higher
		ldr	r2,[r0]		@ grab next block pointer
		@ Apply sanity checks on this entry
		@ 1. Next mod 4 < 2
		@ 2. Last mod 4 = 0
		@ 3. Current + 4 < Last or Next is odd
		@ 4. Next <= HeapEnd
		@ 5. Last < Next or Last <= Next = HeapEnd
		@ 6. map[i] = 0 for Current <= i < Last
		mov	r1,#2
		tst	r2,r1
		bne	cwmBad
		add	r3,r0,#4	@ end of block pointer if islarge = 0
		mov	r1,#1
		tst	r2,r1		@ check islarge bit
		beq	cwmCommon

		sub	r2,#1		@ clear bit
		ldr	r3,[r3]
		mov	r1,#3
		tst	r3,r1
		bne	cwmBad		@ none of these bits should be set
		add	r0,#4
		cmp	r0,r3		@ assert(current + 4 < last)
		bhs	cwmBad		@ bad if not
		sub	r0,#4
cwmCommon:
		ldr	r1,=#HeapEnd
		cmp	r2,r1		@ assert(Next <= HeapEnd)
		bhi	cwmBad		@ bad if not
		bne	cwmLastLtNext	@ if Next!=HeapEnd, check Last < Next
		@ Check Last <= HeapEnd
		cmp	r3,r2		@ assert(Last <= Next)
		bhi	cwmBad		@ bad if not
		b	1f		@ skip check of Last < Next
cwmLastLtNext:	cmp	r3,r2		@ assert(Last < Next)
		bhs	cwmBad
1:
		sub	r3,r5		@ convert Last to index
		sub	r0,r5		@ convert Current to index
		sub	r2,r5		@ convert Next to index
1:		ldrh	r1,[r4,r0]	@ check that map[i]=0 for Cur<=i<Last
		add	r0,#4
		cmp	r1,#0
		bne	cwmBad
		cmp	r0,r3		@ last?
		bne	1b		@ jump if not yet

		ldr	r1,=#RAMmapEnd-RAMmap
		cmp	r0,r1		@ end of array?
		beq	cwmGood		@ finished if so

		@ check that map[i] = 1 for Last <= i < Next

1:		ldrh	r1,[r4,r0]	@ check that map[i]=1 for Last<=i<Next
		add	r0,#4
		cmp	r1,#0
		beq	cwmBad
		cmp	r0,r2
		blo	1b
		mov	r1,r2
		b	cwmNoLeadBlock

cwmBad:		sub	r0,r0
		pop	{r4-r5}
		bx	lr
cwmGood:	mov	r0,#1
		add	r0,#0
		pop	{r4-r5}
		bx	lr

compareMaps:	@ Strategy 2: Create a parallel map based on the free list
		@ then compare the maps. If they differ, there's an error.
		ldr	r0,=#RAMmap2
		ldr	r1,=#1
		ldr	r2,=#NWords*4
1:		stmia	r0!,{r1}
		sub	r2,#4
		bne	1b

		ldr	r0,=#FreeList
		ldr	r0,[r0]
		ldr	r1,=#3
		tst	r0,r1		@ First pointer mod 4 = 0?
		bne	cmBad		@ Jump if not
		ldr	r1,=#HeapOrg
		cmp	r0,r1
		blo	cmBad
		ldr	r1,=#HeapEnd
		cmp	r0,r1
		bhi	cmBad
cmNext:
		ldr	r1,=#HeapEnd
		cmp	r0,r1
		beq	cmGood
		ldr	r2,[r0]		@ Next
		add	r3,r0,#4	@ EOB = Current + 4
		ldr	r1,=#2
		tst	r2,r1
		bne	cmBad		@ bit 1 should be clear
		ldr	r1,=#1
		tst	r2,r1
		beq	cmGotSize	@ If not islarge, skip checks
		sub	r2,#1		@ Clear islarge
		@ Validation of EOB
		ldr	r1,=#HeapEnd	@ there must be room for EOB
		cmp	r3,r1
		bhs	cmBad
		ldr	r3,[r3]		@ Grab EOB
		ldr	r1,=#3
		tst	r3,r1		@ EOB mod 4 = 0?
		bne	cmBad		@ Jump if not
		cmp	r3,r0		@ EOB <= Current?
		bls	cmBad		@ bad if so
		sub	r3,#4		@ we're in islarge mode
		cmp	r3,r0		@ EOB <= Current + 4?
		bls	cmBad		@ bad if so
		add	r3,#4		@ restore original value

cmGotSize:
		@ Implement:
		@ if EOB > HeapEnd: Error
		@ If EOB = HeapEnd:
		@    if Next != HeapEnd: Error
		@ else if EOB >= Next: Error

		ldr	r1,=#HeapEnd
		cmp	r3,r1		@ EOB > HeapEnd?
		bhi	cmBad		@ bad if so
		bne	cmNextChk	@ jump if EOB < HeapEnd
		cmp	r2,r1		@ Next should be = HeapEnd
		bne	cmBad		@ too, else bad
		b	cmSizeOk	@ Size checks passed
cmNextChk:	cmp	r3,r2
		bhs	cmBad		@ Err if EOB >= Next
cmSizeOk:
		cmp	r2,r0		@ Next <= Cur?
		bls	cmBad		@ Bad if so
		cmp	r2,r1		@ Next > HeapEnd?
		bhi	cmBad		@ Bad if so

		ldr	r1,=#HeapOrg
		sub	r0,r1
		blo	cmBad
		sub	r3,r1
		ldr	r1,=#RAMmap2
		add	r0,r1
		add	r3,r1
		ldr	r1,=#0
1:		stmia	r0!,{r1}
		cmp	r0,r3
		blo	1b
		mov	r0,r2		@ Next
		b	cmNext
cmBad:		sub	r0,r0
		bx	lr
cmGood:		ldr	r2,=#RAMmap
		ldr	r3,=#RAMmap2
1:		ldr	r0,[r2]
		add	r2,#4
		ldr	r1,[r3]
		add	r3,#4
		cmp	r0,r1
		bne	cmBad
		ldr	r1,=#RAMmapEnd
		cmp	r2,r1
		blo	1b
		mov	r0,#1
		add	r0,#0
		bx	lr

Check:		push	{lr}
		bl	compareWithMap
		add	r0,#0		@ clears carry
		beq	Fatal
		bl	compareMaps
		add	r0,#0		@ clears carry
		beq	Fatal

		push	{r4-r7}
		ldr	r0,=#RAMmap
		ldr	r1,=#VRAM+240*2+4
2:		ldr	r2,[r0]
		ldr	r3,=#0b00011100111001110001110011100111
		cmp	r2,#0
		beq	1f
		ldr	r3,=#0b00111101111011110011110111101111
1:		stmia	r1!,{r3}
		@stmia	r1!,{r3}
		add	r0,#4
		ldr	r3,=#RAMmapEnd
		cmp	r0,r3
		bne	1f
		ldr	r3,=#480-4*NWords
		add	r1,r3
1:		ldr	r3,=#RAMmap2+4*NWords
		cmp	r0,r3
		blo	2b
		ldr	r0,=#REG_KEYINPUT
1:		ldr	r1,[r0]
		ldr	r2,=#1<<2
		tst	r1,r2
		beq	2f
		ldr	r2,=#1<<3
		tst	r1,r2
		bne	1b
1:		ldr	r1,[r0]
		tst	r1,r2
		beq	1b
2:
		pop	{r4-r7}

		pop	{r0}
		bx	r0

Fatal:		mov	r3,#31
FillScrAndHalt:	ldr	r1,=#VRAM+240*2*60
		mov	r0,r3
		ldr	r2,=#VRAM+240*2*100
1:		strh	r0,[r1]
		add	r1,#2
		cmp	r1,r2
		blo	1b

		.balign	4
		swi	3
		b	.-2

GetMemChk:	push	{r4,lr}
		mov	r4,r0
		bl	GetMem
		mov	r1,r4
		mov	r4,r0
		mov	r0,r1
		bl	MapGetMem
		ldr	r1,=#RAMmapEnd-HeapEnd
		sub	r0,r1
		cmp	r0,r4
		bne	gmcFatal
		mov	r4,r0
		bl	Check
		mov	r0,r4
		pop	{r4}
		pop	{r1}
		bx	r1
gmcFatal:	add	sp,#8
		b	Fatal

FreeMemChk:	push	{r4-r5,lr}
		mov	r4,r0
		mov	r5,r1
		bl	MapFreeMemQ
		bcs	fmcFatal
		mov	r0,r4
		mov	r1,r5
		bl	MapFreeMem
		mov	r0,r4
		mov	r1,r5
		bl	FreeMem
		bl	Check
		pop	{r4-r5}
		pop	{r0}
		bx	r0

fmcFatal:	add	sp,#12
		b	Fatal

		.ltorg

