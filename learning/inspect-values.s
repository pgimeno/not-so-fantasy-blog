		.cpu arm7tdmi
		.text
		.global	_start

		.arm
_start:
		b realstart

		.include "header.inc"
joybus:		swi	3		@ Unhandled here - stop
		b	joybus

		.include "gba_constants.inc"
		.include "gba_moreconstants.inc"

realstart:	.arm
		mov	r3,sp
		mrs	r4,cpsr
		mov	r0,r4
		bic	r0,#0x1F
		orr	r0,#0x11
		msr	cpsr,r0
		mov	r1,sp
		add	r0,#1
		msr	cpsr,r0
		mov	r2,sp
		add	r0,#1
		msr	cpsr,r0
		mov	r5,sp
		add	r0,#4
		msr	cpsr,r0
		mov	r6,sp
		add	r0,#4
		msr	cpsr,r0
		mov	r7,sp
		add	r0,#4
		msr	cpsr,r0
		stmdb	sp!,{r1,r2,r5,r6,r7}	@ aka push

		add	r0,pc,#1
		bx	r0		@ Switch to Thumb mode
		.thumb

		ldr	r1,=#REG_DISPCNT
		ldr	r0,=#DC_BG2EN | DC_FRAME0 | DC_MODE_3
		str	r0,[r1]

		ldr	r6,=#VRAM + (240*8*0)*2 + 0
		ldr	r7,=#Txt.SP
		ldr	r5,=#0x7FFF	@ choose a colour, in this case white
		bl	render_text

		mov	r7,r3
		bl	hex_str

		ldr	r7,=#NumBuf
		bl	render_text


		ldr	r6,=#VRAM+(240*8*1)*2
		ldr	r7,=#Txt.CPSR
		bl	render_text
		mov	r7,r4
		bl	hex_str
		ldr	r7,=#NumBuf
		bl	render_text

		pop	{r3}

		ldr	r6,=#VRAM+(240*8*3)*2
		ldr	r7,=#Txt.SPfiq
		bl	render_text
		mov	r7,r3
		bl	hex_str
		ldr	r7,=#NumBuf
		bl	render_text

		pop	{r3}

		ldr	r6,=#VRAM+(240*8*2)*2
		ldr	r7,=#Txt.SPirq
		bl	render_text
		mov	r7,r3
		bl	hex_str
		ldr	r7,=#NumBuf
		bl	render_text

		pop	{r3}

		ldr	r6,=#VRAM+(240*8*4)*2
		ldr	r7,=#Txt.SPsup
		bl	render_text
		mov	r7,r3
		bl	hex_str
		ldr	r7,=#NumBuf
		bl	render_text

		pop	{r3}

		ldr	r6,=#VRAM+(240*8*5)*2
		ldr	r7,=#Txt.SPabo
		bl	render_text
		mov	r7,r3
		bl	hex_str
		ldr	r7,=#NumBuf
		bl	render_text

		pop	{r3}

		ldr	r6,=#VRAM+(240*8*6)*2
		ldr	r7,=#Txt.SPund
		bl	render_text
		mov	r7,r3
		bl	hex_str
		ldr	r7,=#NumBuf
		bl	render_text

		@ Finish
		b	.
		.ltorg

Txt.CPSR:	.asciz	"  CPSR="
Txt.SP:		.asciz	"SP SYS="
Txt.SPirq:	.asciz	"SP IRQ="
Txt.SPfiq:	.asciz	"SP FIQ="
Txt.SPsup:	.asciz	"SP SUP="
Txt.SPabo:	.asciz	"SP ABT="
Txt.SPund:	.asciz	"SP UND="

		.include "render-text.inc"
