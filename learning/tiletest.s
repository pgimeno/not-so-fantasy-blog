		.cpu arm7tdmi
		.text
		.global	_start

		.arm
_start:
		b realstart

		.include "header.inc"

joybus:		.arm
		swi	3		@ Unhandled here - stop
		b	joybus

		.include "gba_constants.inc"
		.include "gba_moreconstants.inc"

myBG0		= VRAM+0xE000
myBG1		= VRAM+0xE800
myBG2		= VRAM+0xF000
myBG3		= VRAM+0xF800

		.balign 4
ISR:		.arm
		bx	lr
		.ltorg

realstart:	.arm
		add	r0,pc,#1
		bx	r0		@ Switch to Thumb mode
		.thumb

		ldr	r1,=#REG_BG0CNT
		@ 16-bit palette, tileimage base @0400C000, priority 3 (lowest),
		@ tilemap base @04000000, screen size=256x256
		ldr	r0,=#RSIZE_256x256|MAP_E000|BG_PAL16|TLIMG_C000|BG_PRIO_0
		strh	r0,[r1]
		add	r1,#2
		ldr	r0,=#RSIZE_256x256|MAP_E800|BG_PAL16|TLIMG_C000|BG_PRIO_1
		strh	r0,[r1]
		add	r1,#2
		ldr	r0,=#RSIZE_256x256|MAP_F000|BG_PAL16|TLIMG_C000|BG_PRIO_2
		strh	r0,[r1]
		add	r1,#2
		ldr	r0,=#RSIZE_256x256|MAP_F800|BG_PAL16|TLIMG_C000|BG_PRIO_3
		strh	r0,[r1]

		@ Set mode 0 and enable BG 0-3
		ldr	r1,=#REG_DISPCNT
		ldr	r0,=#DC_BG3EN|DC_BG2EN|DC_BG1EN|DC_BG0EN|DC_OBJEN|DC_OBJ_2D|DC_MODE_0
		strh	r0,[r1]

		ldr	r0,=#tileimage
		ldr	r1,=#BG_VRAM+0x0C000
		ldr	r2,=#(256>>1)*64	@ 256/2 bytes per scanline
		bl	memcpy16

		@ tileimage + 256 tiles = sprite memory
		ldr	r0,=#tileimage+0x20*256	@ skip 256 tiles
		ldr	r1,=#OBJ_VRAM0
		ldr	r2,=#(256>>1)*64	@ 256/2 bytes per scanline
		bl	memcpy16

		ldr	r0,=#palette
		ldr	r1,=#BG_PLTT
		ldr	r2,=#(16*2)		@ 16 entries, 2 bytes per entry
		bl	memcpy16

		ldr	r0,=#palette
		ldr	r1,=#OBJ_PLTT		@ copy also to the OBJ palette
		ldr	r2,=#(16*2)
		bl	memcpy16

		@ Fill all mapdata with 0's
		ldr	r0,=#0x00000000
		ldr	r1,=#myBG0
		mov	r2,r0
		stmia	r1!,{r0,r2}
		stmia	r1!,{r0,r2}
		ldr	r0,=#myBG0
		ldr	r2,=#4*(32*32*2)-16	@ all four BG's
		bl	memcpy16

		@ Fill all OAM with OAM0_DISABLE
		ldr	r0,=#OAM0_DISABLE
		ldr	r2,=#OAM2_PRIO_0
		ldr	r1,=#OAM
		stmia	r1!,{r0,r2}
		stmia	r1!,{r0,r2}
		ldr	r0,=#OAM
		ldr	r2,=#128*8-16
		bl	memcpy16		@ disable all sprites

		@ Fill BG3 with FF's
		ldr	r0,=#0x00FF00FF
		ldr	r1,=#myBG3
		ldr	r2,=#32*32		@ size in tiles
1:		stmia	r1!,{r0}
		sub	r2,#2			@ 2 tiles at a time
		bhi	1b

		@ Set a single tile in BG2 and BG1 to check layering priorities
		ldr	r0,=#0x0001
		ldr	r1,=#myBG2
		strh	r0,[r1]
		ldr	r1,=#myBG1
		strh	r0,[r1]

		@ Translate BG2 to (-5, -2)
		ldr	r1,=#REG_BG2HOFS
		ldr	r0,=#-5
		strh	r0,[r1]
		add	r1,#REG_BG2VOFS-REG_BG2HOFS
		ldr	r0,=#-2
		strh	r0,[r1]

		@ Translate BG1 to (-2, -1)
		ldr	r1,=#REG_BG1HOFS
		ldr	r0,=#-2
		strh	r0,[r1]
		add	r1,#REG_BG1VOFS-REG_BG1HOFS
		ldr	r0,=#-1
		strh	r0,[r1]

		@ Finally, display a sprite
		ldr	r1,=#OAM+(0*4+0)*2
		ldr	r0,=#OAM0_VERT|OAM0_PAL16|80-3
		strh	r0,[r1]
		add	r1,#2		@ =#OAM+(0*4+1)*2
		ldr	r0,=#OAM1_SIZE_0|120-4	@ VERT SIZE_0 = 1x2 tiles
		strh	r0,[r1]
		add	r1,#2		@ =#OAM+(0*4+2)*2
		ldr	r0,=#OAM2_PRIO_0|255-32	@ tile number
		strh	r0,[r1]

		@ Finish
		b	.
		.ltorg

@ copy 16 bytes at a time, length must be a multiple of 16
memcpy16:	push	{r3-r6}
1:
		ldmia	r0!,{r3-r6}
		stmia	r1!,{r3-r6}
		sub	r2,#16
		bhi	1b

		pop	{r3-r6}
		bx	lr
		.ltorg

		.balign	4
palette:	.incbin	"palette.bin"
tileimage:	.incbin	"tileimage.bin"
