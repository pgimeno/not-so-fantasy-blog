import sys

def rgb8to5(b, g, r):
  #s = int(s, 16)
  #r, g, b = s >> 16 & 0xFF, s >> 8 & 0xFF, s & 0xFF

  # scale to max range supported
  s = float(0xFB)/255.
  r, g, b = r*s, g*s, b*s

  # apply rounding to nearest or even
  s = float(1<<55)
  r = int(r + s - s)
  g = int(g + s - s)
  b = int(b + s - s)

  #return ('00000'+hex((r<<16)+(g<<8)+b)[2:])[-6:]
  return (b << 7) + (g << 2) + (r >> 3)


def main():
  f = open(sys.argv[1], 'rb')
  header = bytearray(f.read(18))
  try:
    # Test header bits
    if header[0] != 0:
      raise Exception("Not a TGA file, or identification field not empty")

    if header[1] != 1 or header[2] != 1:
      raise Exception("Not a TGA uncompressed palette image")

    if (header[17] & (1<<5)) == 0:
      raise Exception("TGA data format is bottom-up, not top-down")

    if header[12] + 256*header[13] != 256:
      raise Exception("Width should be 256 px")

    imgh = header[14] + 256*header[15]
    if imgh > 192:
      raise Exception("Image too high, maximum 192 vertical px")

    if (imgh & 7) != 0:
      raise Exception("Vertical height not a multiple of 8")

    if header[7] != 24:
      raise Exception("Sorry, currently I can only handle 24-bit palettes")

    if header[3] + 256*header[4] != 0 or header[5] + 256*header[6] != 16:
      raise Exception("Sorry, currently I can only handle palettes defined for colours 0-15")

    palette = bytearray(f.read(48))
    bitmap = bytearray(f.read(256*imgh))

    if any((i & ~0xF) != 0 for i in bitmap):
      raise Exception("Palete index > 15 detected")

  finally:
    f.close()

  # Reroganize palette
  outpal = bytearray()
  for i in range(0, len(palette), 3):
    entry = rgb8to5(*palette[i:i+3])
    outpal.append(entry & 0xFF)
    outpal.append(entry >> 8)

  f = open('palette.bin', 'wb')
  f.write(outpal)
  f.close()

  outdata = bytearray()
  for y in range(imgh>>3):  # As many tiles vertically as the image has
    for x in range(32):  # 32 tiles horizontally
      for ty in range(8):  # 8 rows per tile
        for tx in range(0, 8, 2):  # 8 pixels per tile, 2 pixels per byte
          ofs = y*256*8 + x*8 + ty*256 + tx
          outdata.append(bitmap[ofs] + bitmap[ofs+1]*16)

  f = open('tileimage.bin', 'wb')
  f.write(outdata)
  f.close()

  sys.stderr.write("Successfully generated palette.bin and tileimage.bin\n")

if __name__ == "__main__":
  ret = main()
  if ret:
    sys.exit(ret)
