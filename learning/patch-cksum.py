import sys
def main():
  f = open(sys.argv[1], 'r+b')
  data = bytearray(f.read(0xBD))
  data.append((0xE7-sum(data[0xa0:0xBD])) & 0xFF)
  f.write(data[-1:])
  f.close()

main()
