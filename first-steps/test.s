		.cpu arm7tdmi
		.text
		.global	_start

DISPCNT		= 0x04000000		@ LCD Control
DISPSTAT	= 0x04000004		@ General LCD Status (STAT,LYC)

VRAM		= 0x06000000

FRAME_SEL_BIT	= 0x10
BG2_ENABLE	= 0x400


		.arm
_start:
		add	r0,pc,#1
		bx	r0		@ Switch to Thumb mode

		.thumb

		ldr	r7,=#DISPCNT
		ldr	r0,=#(BG2_ENABLE | 3) & ~FRAME_SEL_BIT
		str	r0,[r7]
		add	r7,r7,#DISPSTAT-DISPCNT

		mov	r1,#0x8
		ldr	r0,[r7]
		orr	r0,r1
		str	r0,[r7]


		mov	r5,#0x3F
		mov	r0,r5		@ pixel value

		@ Block copy with 4 registers in STMIA
		ldr	r7,=#VRAM
		mov	r1,r0
		lsl	r0,#16
		orr	r0,r0,r1
		mov	r1,r0
		mov	r2,r0
		mov	r3,r0

		@ deliberately leave some pixels out
		ldr	r6,=#240*160-8

.L1:		stmia	r7!,{r0,r1,r2,r3}
		sub	r6,r6,#8
		bhi	.L1

		mov	r5,#0x3F
		ldr	r4,=#0x3F	@ mask
.L3:
		mov	r0,r5		@ pixel value
		and	r0,r4

		@ Block copy with 4 registers in STMIA
		ldr	r7,=#VRAM
		mov	r1,r0
		lsl	r0,#16
		orr	r0,r0,r1
		mov	r1,r0
		mov	r2,r0
		mov	r3,r0

		ldr	r6,=#240*80

.L2:
		stmia	r7!,{r0,r1,r2,r3}
		sub	r6,r6,#8
		bhi	.L2

		sub	r5,r5,#1
		b	.L3

		@ Stop here
end:		b	end

		.ltorg
